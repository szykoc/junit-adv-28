package figures;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RectangleTest {

    private Figure rectangle;

    @BeforeEach
    void setup() {
        rectangle = new Rectangle(5, 2);
    }

    @Test
    @DisplayName("should calculate area of rectangle")
    void test1() {
        //when
        int area = rectangle.area();
        //then
        assertEquals(10, area);
    }

    @Test
    @DisplayName("should calculate circuit of rectangle")
    void test2(){
        //when
        int circuit = rectangle.circuit();
        //then
        assertEquals(14, circuit);
    }

}