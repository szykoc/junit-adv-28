package figures;

import org.junit.jupiter.api.*;


import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class SquareTest {

    private Figure square;

    @BeforeEach // przed kazdym testem
    void setup() {
        square = new Square(4);
        System.out.println("Before each test");
    }

    @AfterEach
    void after() {
        System.out.println("After each test");
    }

    @BeforeAll
    static void beforeAll() {
        System.out.println("Before all tests");
    }

    @AfterAll
    static void afterAll() {
        System.out.println("After all tests");
    }

    @Test
    @DisplayName("should calcuate are square") // junit5
    void test1() {
        // given
        Figure square = new Square(5);
        // when
        int area = square.area();
        // then
        assertEquals(25, area);  // junit5
        assertThat(25).isEqualTo(area);
    }

    @Test
    @DisplayName("should calculate circuit of square")
    void test2() {
        //given
        Figure square = new Square(10);
        //when
        int circuit = square.circuit();
        //then
        assertEquals(40, circuit);
        assertThat(40).isEqualTo(circuit);
    }

}