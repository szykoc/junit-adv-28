package weatherMock;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class WeatherServiceTest {

    @InjectMocks
    private WeatherService service;

    @Mock
    private Temperatures temperatures;

    @Test
    @DisplayName("should return map with predicted temperatures")
        // rozwiazanie z użyciem stuba
    void test1() {
        //given
        Temperatures temperaturesStub = new TemperaturesStub();
        WeatherService service = new WeatherService(temperaturesStub);
        //when
        Map<String, Double> result = service.calculateForecast();
        //then
        assertNotNull(result);
        assertEquals(5, result.size());

        assertAll(
                () -> assertEquals(result.get("Katowice"), 21.5),
                () -> assertEquals(result.get("Gliwice"), 23.0),
                () -> assertEquals(result.get("Warszawa"), 23.0),
                () -> assertEquals(result.get("Wroclaw"), 23.0)
        );
    }

    @Test
    @DisplayName("same as 1 but with mock")
    void test2() {
        //given
        when(temperatures.getTemperatures()).thenReturn(
                Map.of(
                        "Katowice", 20.5,
                        "Rzeszów", 24.2,
                        "Warszawa", 30.0,
                        "Gdańsk", 20.0,
                        "Gliwice", 16.0,
                        "Radom", 44.0)
        );
        when(temperatures.getError(any())).thenReturn(3.0);
        //when
        Map<String, Double> result = service.calculateForecast();
        //then
        verify(temperatures, times(1)).getTemperatures();
        verify(temperatures, times(2)).getError(any());
        assertNotNull(result);
        assertEquals(6, result.size());
    }


}