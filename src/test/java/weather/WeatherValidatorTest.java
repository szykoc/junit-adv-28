package weather;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;

class WeatherValidatorTest {

    WeatherValidator validator = new WeatherValidator();

    @Test
    @DisplayName("when null object is passed to WeatherValidator validate method" +
            "IllegalArgumentException should be thrown")
    void test1() {
        //given
        Weather weather = null;
        //when

        // junit5
        IllegalArgumentException exception = assertThrows(
                IllegalArgumentException.class,
                () -> validator.validate(weather)
        );
        // assertJ
        assertThatThrownBy(() -> validator.validate(weather))
                .hasMessage("Weather can't be null")
                .isInstanceOf(IllegalArgumentException.class);

        // junit4
        try {
            validator.validate(weather);
        } catch (IllegalArgumentException ex) {
            assertEquals("Weather can't be null", ex.getMessage());
        }
        //then
        assertEquals("Weather can't be null", exception.getMessage());
    }

    @ParameterizedTest(name = "when given Weather (temp={0}, wind={1}, rainfall={2}) is passed to WeatherValidator" +
            "validate method IllegalArgumentException should be thrown with message \"{3}\"")
    @DisplayName("exception tests WeatherValidator")
    @CsvSource(value = {
          "-100.01, 0.0, 0.0, Temperature can't be lower than -100.00 °C",
          "100.01, 0.0, 0.0, Temperature can't be higher than 100.00 °C",
          "0.0, 500.01, 0.0, Wind can't be higher than 500.00 km/h",
          "0.0, -0.01, 0.0, Wind can't be negative",
          "0.0, 0.0, -0.01, Rainfall can't be negative",
          "0.0, 0.0, 1000.01, Rainfall can't be higher than 1000.00 mm/day"
    })
    void test2(double temp, double wind, double rainfall, String expectedErrorMessage) {
        //given
        Weather weather = new Weather(temp, wind, rainfall);
        //when
        IllegalArgumentException exception = assertThrows(
                IllegalArgumentException.class,
                () -> validator.validate(weather));
        //then
        assertEquals(expectedErrorMessage, exception.getMessage());

        //assertJ
        //when && then
        assertThatThrownBy(() -> validator.validate(weather))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage(expectedErrorMessage);
    }

    @ParameterizedTest(name = "when given Weather (temp = {0}, wind = {1}, rainfall = {2} is passed to " +
            "WeatherValidator validate method no exceptions should be thrown")
    @DisplayName("happy paths WeatherValidator")
    @CsvFileSource(resources = "/weatherHappyPath.csv", numLinesToSkip = 1)
    void test3(double temp, double wind, double rainfall) {
        //given
        Weather weather = new Weather(temp, wind, rainfall);
        //when && then
        assertDoesNotThrow(() -> validator.validate(weather));
    }


}