package smog;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import smog.exception.NoDataProvidedException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class PmAlarmServiceTest {

    PmAlarmService pmAlarmService = new PmAlarmService();

    @ParameterizedTest(name = "for given PM10 = {0} and country = {1} should return alert type = {2}")
    @DisplayName("alarm levels without exceptions")
    @CsvFileSource(resources = "/pm10AlertData.csv", numLinesToSkip = 1)
    void test1(int pm10, Country country, AlarmLevel expectedAlarmLevel) throws NoDataProvidedException {
        //when
        AlarmLevel alarmMessage = pmAlarmService.getAlarmMessage(pm10, country);
        //then
        assertEquals(expectedAlarmLevel, alarmMessage);
    }

    @ParameterizedTest(name = "for given PM10 = {0} and country = {1} should thrown exception")
    @DisplayName("not supported pm10 values")
    @CsvFileSource(resources = "/pm10AlertDataNotSupported.csv", numLinesToSkip = 1)
    void test2(int pm10, Country country) {
        //when && then
        assertThrows(NoDataProvidedException.class, () -> pmAlarmService.getAlarmMessage(pm10, country));
    }

}