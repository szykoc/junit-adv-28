package figures;

public class Rectangle implements Figure {

    private final int a;
    private final int b;

    public Rectangle(int a, int b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public int area() {
        return a * b;
    }

    @Override
    public int circuit() {
        return 2 * a + 2 * b;
    }
}
