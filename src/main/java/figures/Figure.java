package figures;

public interface Figure {
    int area();
    int circuit();
}
