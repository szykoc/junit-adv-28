package figures;

public class Square implements Figure {

    private int a;

    public Square(int a) {
        this.a = a;
    }

    @Override
    public int area() {
        return a*a;
    }

    @Override
    public int circuit() {
        return 4*a;
    }


}
