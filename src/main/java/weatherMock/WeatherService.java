package weatherMock;

import java.util.HashMap;
import java.util.Map;

public class WeatherService {

    private final Temperatures temperatures;

    public WeatherService(Temperatures temperatures) {
        this.temperatures = temperatures;
    }

    public Map<String, Double> calculateForecast() {
        Map<String, Double> resultMap = new HashMap<>();

        for (Map.Entry<String, Double> temperature : temperatures.getTemperatures().entrySet()) {
            if (temperature.getKey().startsWith("R")){
                Double err = temperatures.getError(temperature.getKey());
                resultMap.put(temperature.getKey(), temperature.getValue() + err);
            } else {
                resultMap.put(temperature.getKey(), temperature.getValue() + 1.0);
            }
        }
        return resultMap;
    }

    // metoda na średnia // pubblic Double getAverave(){}

    // -> wolamy gettemperatues i obliczamy
    // -> dla miast na W nie liczmy

    // metoda na mediane

    // -> wolamy gettemepratues i obliczamy
    // dla miast na R nie liczmy


}
