package weatherMock;

import java.util.Map;

public interface Temperatures {

    Map<String, Double> getTemperatures();
    Double getError(String key);

}
