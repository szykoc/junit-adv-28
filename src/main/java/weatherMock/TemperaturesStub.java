package weatherMock;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TemperaturesStub implements Temperatures {

    @Override
    public Map<String, Double> getTemperatures() {
        // dummy data
        return Map.of(
                "Katowice", 20.5,
                "Gliwice", 22.4,
                "Warszawa", 24.4,
                "Krakow", 21.4,
                "Wroclaw", 21.4
        );
    }

    @Override
    public Double getError(String key) {
        return null;
    }
}
