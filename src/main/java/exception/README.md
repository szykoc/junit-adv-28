#### zadanie


+ Stwórz swój własny Exception o nazwie BookNotAvailableException.
+ Stwórz klasę Bok zawierającą konstruktor, pole: String title, String author i gettery.
+ Stwórz klasę zawierającą logikę wyszukiwania ksiązek:
    - metoda findBook(Book book)
    
+ wewnątrz metody zaimplementuj mapę typu HashMap, której kluczem będzie String przechowujący nazwę ksiązki, 
a wartością Boolean informujący o tym czy można daną ksiązke wypożyczyć

Jeżeli dana ksiązka będzie niedostępna należy rzucić wyjątek BookNotAvailableException.

Napisz testy i obsłuż wyjątek.

