# smog

W tym zadaniu, twoim zadaniem jest rozwinięcie usługi odpowiedzialnej za wysyłanie różnego rodzaju typów alarmów w oparciu o pomiary PM.
Obecnie każdy kraj ma własne limity, definiujące kiedy dany typ powinien być wysyłany do ludzi.
Spójrz na dane zdjęcie:
![.img/477.jpg](.img/477.jpg)
Źródło: [https://www.polskialarmsmogowy.pl/polski-alarm-smogowy/smog/szczegoly,poziomy-informowania-i-alarmowe,19.html](https://www.polskialarmsmogowy.pl/polski-alarm-smogowy/smog/szczegoly,poziomy-informowania-i-alarmowe,19.html)

1. Przejdź do `PmAlarmServiceTest` i przeprowadź trzy testy.
2. Jak można zauważyć, dla każdego testu utworzono instancję `PmAlarmService`.
3. Zastąp wszystkie trzy wystąpienia instancji jedną przy użyciu odpowiedniej adnotacji 
4. Wdrożyć logikę dla 3 krajów z brakujących krajów i pokryj ją odpowiednimi testami.
5. Testy parametryzowane dla poszczególnych krajów.