package smog;

public enum AlarmLevel {
    NONE,
    INFO,
    WARNING
}
