package smog.exception;

public class NoDataProvidedException extends Exception{
    public NoDataProvidedException() {
        super("No data provided for such country...");
    }
}
